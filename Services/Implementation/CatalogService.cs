using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using AutoMapper;
using RoconLibrary.Utility;
using Serilog;

namespace LoadLCOCatalog.Services.Implementation
{
    public class CatalogService : ICatalogService
    {
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        public CatalogService(IConfigurationRoot config, IMapper mapper)
        {
            _config = config;
            _mapper = mapper;
        }

        public void processStart(OracleConnection connection)
        {
            try
            {
                insert(connection);
            } catch(Exception e)
            {
                Log.Error("Error inserting to catalog. Message = {0}. Stacktrace is: {1}", e.Message, e.StackTrace);
            }
            
            try
            {
                update(connection);
            } catch(Exception e)
            {
                Log.Error("Error updated to catalog. Message = {0}. Stacktrace is: {1}", e.Message, e.StackTrace);
            }
        }

        private void insert(OracleConnection connection) {
            ConnectionUtil.openConnection(connection);
            var oraclCmd = connection.CreateCommand();
            var sql = "INSERT INTO lcbo.LCO_ITEM_CATALOG(item, active, SELL_BY_BOTTLE, MAX_UNITS_PER_ORDER, LAST_UPDATE_BY, LAST_UPDATE_DT)"
                    + " SELECT item, CASE active WHEN 'Y' THEN 1 ELSE 0 END active, SELL_BY_BOTTLE,"
                    + " 9999999 MAX_UNITS_PER_ORDER, LAST_UPDATE_BY, LAST_UPDATE_DT"
                    + " FROM LCBO.V_LCO_CATALOG_EVENTS"
                    + " WHERE ACTION = 'ADD' AND trunc(EFFECTIVE_DT) <= trunc(sysdate)";
            Log.Debug($"The insert sql is: [{sql}]");
            oraclCmd.CommandText = sql;
            int updated = oraclCmd.ExecuteNonQuery();
            Log.Debug("Quantity inserted: " + updated);
        }

        private void update(OracleConnection connection) {
            ConnectionUtil.openConnection(connection);
            var oraclCmd = connection.CreateCommand();
            var sql = "MERGE INTO LCBO.LCO_ITEM_CATALOG LCE"
                    + " USING ("
                    + "    SELECT item, CASE active WHEN 'Y' THEN 1 ELSE 0 END active, SELL_BY_BOTTLE, "
                    + "    9999999 MAX_UNITS_PER_ORDER, LAST_UPDATE_BY, LAST_UPDATE_DT"
                    + "    FROM LCBO.V_LCO_CATALOG_EVENTS VLCE" 
                    + "    WHERE ACTION = 'UPDATE' AND trunc(EFFECTIVE_DT) <= trunc(sysdate)) TMP"
                    + " ON (LCE.ITEM = TMP.ITEM)"
                    + " WHEN MATCHED THEN" 
                    + " UPDATE SET" 
                    + "    LCE.active = TMP.active," 
                    + "    LCE.SELL_BY_BOTTLE = TMP.SELL_BY_BOTTLE," 
                    + "    LCE.MAX_UNITS_PER_ORDER = TMP.MAX_UNITS_PER_ORDER," 
                    + "    LCE.LAST_UPDATE_BY = TMP.LAST_UPDATE_BY," 
                    + "    LCE.LAST_UPDATE_DT = TMP.LAST_UPDATE_DT";
            Log.Debug($"The update sql is: [{sql}]");
            oraclCmd.CommandText = sql;
            int updated = oraclCmd.ExecuteNonQuery();
            Log.Debug("Quantity updated: " + updated);
        }
    }
}
            